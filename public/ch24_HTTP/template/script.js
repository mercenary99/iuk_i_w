function createCard(name, birthYear, url) {
  let col = document.createElement("div")
  col.className = "col-sm";

  let card = document.createElement("div")
  card.className = "card";

  let cardBody = document.createElement("div")
  cardBody.className = "card-body";

  let cardTitle = document.createElement("h4");
  cardTitle.className = "card-title";
  cardTitle.appendChild(document.createTextNode(name));

  let cardSubtitle = document.createElement("h4");
  cardSubtitle.className = "card-subtitle mb-2 text-muted";
  cardSubtitle.appendChild(document.createTextNode(birthYear));

  let world = document.createElement("p");
  world.style.display = "none";

  let button = document.createElement("Button");
  button.className = "btn btn-dark";
  button.appendChild(document.createTextNode("Heimat anzeigen"));
  button.onclick = function () {
    button.style.display = "none";
    home (url, world);
  };

  cardBody.appendChild(cardTitle);
  cardBody.appendChild(cardSubtitle);
  cardBody.appendChild(world);
  cardBody.appendChild(button);
  card.appendChild(cardBody);
  col.appendChild(card);

  document.getElementById("people").appendChild(col);
}

let people = "https://swapi.co/api/people/";

fetch(people)
  .then(res => res.json())
  .then(function (data) {
    let object = data.results;
    for (let i = 0; i < object.length; i++) {
      createCard(object[i].name, object[i].birth_year, object[i].homeworld);
    }
  });

function home(url, world) {
  fetch(url)
    .then(res => res.json())
    .then(function (data) {
      world.innerHTML = "Heimat: " + data.name;
      world.style.display = "inline";
    })
}

